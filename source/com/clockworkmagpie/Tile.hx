/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 * Fluyo is distributed under the terms of the GNU General Public License
 */

package com.clockworkmagpie;

import org.flixel.FlxG;
import org.flixel.FlxSprite;

class Tile extends FlxSprite
{
    static inline var BLOCK_TYPES : Int = 6;
    static public inline var BAD_BLOCK_TYPE : Int = 6;
    static public inline var DEATH_ANIMATION_DT : Float = 0.5;

    private var mDeathTimer : Float;

    public var type(default, null) : Int;

    public var isBad(get, never) : Bool;

    public function new(?blockType : Int)
    {
        super();

        if( blockType == null )
        {
            blockType = Std.random(BLOCK_TYPES);
        }
        type = blockType;

        loadGraphic("assets/blocks.png", true, false, 20, 20);
        frame = type;
    }

    inline public function remove()
    {
        alive = false;
        frame = type + BLOCK_TYPES + 1;
        mDeathTimer = DEATH_ANIMATION_DT;
    }

    override public function update()
    {
        super.update();

        if(!alive)
        {
            mDeathTimer -= FlxG.elapsed;
            if(mDeathTimer <= 0)
            {
                Gbl.burst(this);
                kill();
            }
        }
    }

    inline private function get_isBad() : Bool
    {
        return type == BAD_BLOCK_TYPE;
    }
}
