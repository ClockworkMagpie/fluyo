/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 * Fluyo is distributed under the terms of the GNU General Public License
 */

package com.clockworkmagpie.input;

import org.flixel.FlxBasic;

class Input extends FlxBasic
{
    public var left(default, null) : Bool;
    public var right(default, null) : Bool;
    public var rotate(default, null) : Bool;
    public var drop(default, null) : Bool;
}
