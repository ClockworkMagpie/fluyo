/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 * Fluyo is distributed under the terms of the GNU General Public License
 */

package com.clockworkmagpie.input;

import org.flixel.FlxG;

class KeyboardInput extends Input
{
    private var mKeys : Keys;
    
    public function new(keys : Keys)
    {
        super();
        mKeys = keys;
    }

    override public function update()
    {
        super.update();
        rotate = FlxG.keys.justPressed(mKeys.rotate);
        left = FlxG.keys.justPressed(mKeys.left);
        right = FlxG.keys.justPressed(mKeys.right);
        drop = FlxG.keys.pressed(mKeys.drop);
    }
}
