/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 * Fluyo is distributed under the terms of the GNU General Public License
 */

package com.clockworkmagpie.input;

import flash.geom.Point;
import flash.geom.Rectangle;

import org.flixel.FlxG;
import org.flixel.system.input.FlxTouch;
import org.flixel.system.input.FlxTouchManager;

class TouchInput extends Input
{
    // Actions won't happen if the touch is within this many pixels of the
    // origin
    static inline var DEAD_ZONE : Float = 20;

    // Actions are toggled off for this many seconds before being repeated
    static inline var REPEAT_DELAY : Float = 0.3;
    
    private var mBounds : Rectangle;
    private var mActiveTouch : FlxTouch;
    private var mTouchOrigin : Point;
    private var mRepeatDelay : Float;
    
    public function new(bounds : Rectangle)
    {
        super();

        mBounds = bounds.clone();
        mTouchOrigin = new Point();
        mRepeatDelay = 0;
    }

    override public function update()
    {
        super.update();

        left = false;
        right = false;
        rotate = false;
        drop = false;

        // If we're tracking a touch point, update it
        if(mActiveTouch != null)
        {
            mRepeatDelay -= FlxG.elapsed;
            
            if(!mActiveTouch.pressed())
            {
                mActiveTouch = null;
            }
            else
            {
                var repeatOkay = (mRepeatDelay <= 0);
                // try to track gesture
                var dx = mActiveTouch.x - mTouchOrigin.x;
                var dy = mActiveTouch.y - mTouchOrigin.y;

                if(Math.abs(dx) < DEAD_ZONE)
                {
                    rotate = repeatOkay && (dy < -DEAD_ZONE);
                    drop = (dy > DEAD_ZONE);
                }
                else if(Math.abs(dy) < DEAD_ZONE)
                {
                    left = repeatOkay && (dx < -DEAD_ZONE);
                    right = repeatOkay && (dx > DEAD_ZONE);
                }

                if(rotate || left || right)
                {
                    mRepeatDelay = REPEAT_DELAY;
                }
            }
        }

        // If we're not tracking a touch point now, see if we have a new one
        if(mActiveTouch == null)
        {
            for(touch in FlxG.touchManager.touches)
            {
                if(touch.pressed() && mBounds.contains(touch.x, touch.y))
                {
                    mActiveTouch = touch;
                    mTouchOrigin.x = touch.x;
                    mTouchOrigin.y = touch.y;
                    mRepeatDelay = 0;
                    break;
                }
            }
        }
    }
}
