/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 * Fluyo is distributed under the terms of the GNU General Public License
 */

package com.clockworkmagpie.input;

class Keys
{
    public var left : String;
    public var right : String;
    public var rotate : String;
    public var drop : String;

    public function new()
    {
    }

    static public function wasd()
    {
        var controls = new Keys();
        controls.left = "A";
        controls.right = "D";
        controls.rotate = "W";
        controls.drop = "S";
        return controls;
    }

    static public function ijkl()
    {
        var controls = new Keys();
        controls.left = "J";
        controls.right = "L";
        controls.rotate = "I";
        controls.drop = "K";
        return controls;
    }
}
