/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 * Fluyo is distributed under the terms of the GNU General Public License
 */

package com.clockworkmagpie;

import nme.Assets;
import flash.geom.Rectangle;
import nme.net.SharedObject;
import org.flixel.FlxButton;
import org.flixel.FlxG;
import org.flixel.FlxPath;
import org.flixel.FlxSave;
import org.flixel.FlxSprite;
import org.flixel.FlxState;
import org.flixel.FlxText;

import com.clockworkmagpie.input.KeyboardInput;
import com.clockworkmagpie.input.Keys;
import com.clockworkmagpie.input.TouchInput;

class PlayState extends FlxState
{
    private var mBoard1 : Board;
    private var mBoard2 : Board;
    private var mWaitingToStart : Bool;
    private var mWinnerText : FlxText;
    private var mLoserText : FlxText;
    private var mReplayText : FlxText;
    
	override public function create():Void
	{
        mBoard1 = new Board();
        mBoard2 = new Board();
        
        mBoard1.x = Std.int(0.25 * FlxG.width - 0.5 * mBoard1.width);
        mBoard1.y = Std.int(0.5 * FlxG.height - 0.5 * mBoard1.height);

        mBoard2.x = Std.int(0.75 * FlxG.width - 0.5 * mBoard2.width);
        mBoard2.y = mBoard1.y;

        #if mobile
        mBoard1.input = new TouchInput(new Rectangle(mBoard1.x, mBoard1.y,
                                                     mBoard1.width, mBoard1.height));
        mBoard2.input = new TouchInput(new Rectangle(mBoard2.x, mBoard2.y,
                                                     mBoard2.width, mBoard2.height));
        #else
        mBoard1.input = new KeyboardInput(Keys.wasd());
        mBoard2.input = new KeyboardInput(Keys.ijkl());
        #end
        
        var bg = new FlxSprite(mBoard1.x, mBoard1.y);
        bg.makeGraphic(mBoard1.width, mBoard1.height, 0xFF404040);
        add(bg);

        bg = new FlxSprite(mBoard2.x, mBoard2.y);
        bg.makeGraphic(mBoard2.width, mBoard2.height, 0xFF404040);
        add(bg);

        mBoard1.active = false;
        mBoard2.active = false;

        add( mBoard1 );
        add( mBoard2 );

        Gbl.init();
        add(Gbl.burstEmitters);

        // Create the text fields for the end game screen
        mWinnerText = new FlxText(0, 0, mBoard1.width);
        mLoserText = new FlxText(0, 0, mBoard1.width);

        mWinnerText.shadow = mLoserText.shadow = 0xFF000000;
        mWinnerText.useShadow = mLoserText.useShadow = true;

        mWinnerText.y = mBoard1.y + 0.3 * mBoard1.height - 0.5 * mWinnerText.height;
        mLoserText.y = mBoard1.y + 0.3 * mBoard1.height - 0.5 * mLoserText.height;

        mReplayText = new FlxText(0, 0, FlxG.width);
        #if mobile
        mReplayText.text = "-Tap to play-";
        #else
        mReplayText.text = "-Press SPACE to play-";
        #end
        mReplayText.size = 16;
        mReplayText.alignment = "center";
        mReplayText.shadow = 0xFF181818;
        mReplayText.useShadow = true;
        mReplayText.y = 0.8 * FlxG.height - 0.5 * mReplayText.height;
        add(mReplayText);

        showInstructions();

        mWaitingToStart = true;
	}
	
	override public function destroy():Void
	{
		super.destroy();
	}

	override public function update():Void
	{
		super.update();

        if(mWaitingToStart)
        {
            if(FlxG.keys.justPressed("SPACE") || FlxG.mouse.justPressed())
            {
                remove(mWinnerText);
                remove(mLoserText);
                remove(mReplayText);
                mBoard1.replay();
                mBoard2.replay();
                mWaitingToStart = false;
            }
        }
        else
        {
            if(mBoard1.lost || mBoard2.lost)
            {
                mBoard1.active = false;
                mBoard2.active = false;
                showEndScreen();
                mWaitingToStart = true;
            }
        }
	}

    public function addBadTiles(source : Board, number : Int)
    {
        for(board in [mBoard1, mBoard2])
        {
            if(board != source)
            {
                board.addBadBlocks(number);
            }
        }
    }

    private function showEndScreen()
    {
        mWinnerText.size = mLoserText.size = 24;
        mWinnerText.useShadow = mLoserText.useShadow = true;
        mWinnerText.alignment = mLoserText.alignment = "center";

        mWinnerText.text = "Winner";
        mLoserText.text = "Loser";

        if(mBoard1.lost)
        {
            mLoserText.x = mBoard1.x;
            mWinnerText.x = mBoard2.x;
        }
        else
        {
            mWinnerText.x = mBoard1.x;
            mLoserText.x = mBoard2.x;
        }

        #if mobile
        mReplayText.text = "-Tap to play again-";
        #else
        mReplayText.text = "-Press SPACE to play again-";
        #end
        
        add(mWinnerText);
        add(mLoserText);
        add(mReplayText);
    }

    private function showInstructions()
    {
        // Reuse the win/lose text fields here, because they already fit

        #if mobile
        mWinnerText.text = mLoserText.text = "Swipe to move, up rotates";
        #else
        mWinnerText.text = "A - left\nD - right\nW - rotate\nS - drop";
        mLoserText.text = "J - left\nL - right\nI - rotate\nK - drop";
        #end

        mWinnerText.alignment = mLoserText.alignment = "left";
        mWinnerText.size = mLoserText.size = 8;
        mWinnerText.x = mBoard1.x + 15;
        mLoserText.x = mBoard2.x + 15;
        add(mWinnerText);
        add(mLoserText);
    }
}
