/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 * Fluyo is distributed under the terms of the GNU General Public License
 */

package com.clockworkmagpie;

import haxe.ds.IntMap;
import org.flixel.FlxG;
import org.flixel.FlxGroup;
import org.flixel.util.FlxTimer;
import org.flixel.tweens.FlxTween;
import org.flixel.tweens.util.Ease;

import com.clockworkmagpie.input.Input;
import com.clockworkmagpie.tweens.FixedLinearMotion;

private typedef Position =
{
    var x : Float;
    var y : Float;
}

private typedef CellPosition =
{
    var row : Int;
    var column : Int;
}

class Board extends FlxGroup
{
    static inline var TILE_WIDTH : Int = 20;
    static inline var COLUMNS : Int = 6;
    static inline var ROWS : Int = 12;
    static inline var FALLING_SPEED : Float = 30;
    static inline var DROP_DT : Float = 1;
    static inline var BAD_BLOCK_DELAY : Float = 5;

    public var width : Int = TILE_WIDTH * COLUMNS;
    public var height : Int = TILE_WIDTH * ROWS;

    public var x : Int;
    public var y : Int;

    public var lost : Bool;

    public var input(default, set) : Input;

    private var mTiles : Array<Tile>;
    private var mFalling : Array<Tile>;
    private var mMatchTimer : FlxTimer;
    private var mDoingMatches : Bool;
    private var mChainCount : Int;
    private var mBadBlocksWaiting : Int;
    private var mBadBlockTime : Float;
    private var mBadBlockLastColumn : Int;

    public function new()
    {
        super();
        mTiles = new Array<Tile>();
        mFalling = new Array<Tile>();
        mMatchTimer = new FlxTimer();
        mDoingMatches = false;
        mBadBlocksWaiting = 0;
        mBadBlockLastColumn = 0;
        lost = false;
    }

    private function set_input(inp : Input)
    {
        if(input != null)
        {
            remove(input);
        }
        
        input = inp;
        add(input);
        return inp;
    }

    private inline function index(row, column)
    {
        return column + COLUMNS * row;
    }

    private inline function pos(row, column)
    {
        return {x: Math.floor(x + column * TILE_WIDTH),
                y: Math.floor(y + row * TILE_WIDTH)};
    }

    private inline function cellPosition(tx : Float, ty : Float)
    {
        return {row: Math.floor((ty - y) / TILE_WIDTH),
                column: Math.floor((tx - x) / TILE_WIDTH)};
    }

    public function replay()
    {
        for(ii in 0...mTiles.length)
        {
            if(mTiles[ii] != null)
            {
                mTiles[ii].kill();
            }
            mTiles[ii] = null;
        }
        for(ii in 0...mFalling.length)
        {
            if(mFalling[ii] != null)
            {
                mFalling[ii].kill();
            }
            mFalling[ii] = null;
        }
        mMatchTimer.stop();
        mDoingMatches = false;
        mChainCount = 0;
        mBadBlocksWaiting = 0;
        mBadBlockLastColumn = 0;
        mBadBlockTime = 0;
        lost = false;        
        active = true;
    }

    public function addPair()
    {
        mDoingMatches = false;

        if(mBadBlockTime <= 0 && mBadBlocksWaiting > 0)
        {
            dropBadBlocks();
        }
        else
        {
            var column = Std.int(COLUMNS / 2);
            
            if(mTiles[index(0, column)] != null)
            {
                lost = true;
                return;
            }
            
            for(ii in 0...2)
            {
                mFalling[ii] = new Tile();
                var position = pos(-1 - ii, column);
                mFalling[ii].x = position.x;
                mFalling[ii].y = position.y;
                add(mFalling[ii]);
            }
        }
    }

    override public function update()
    {
        super.update();

        if(lost)
        {
            return;
        }

        var haveFallingBlock = false;

        var horizontalMove : Int = 0;
        var speed = FALLING_SPEED;

        if(input.rotate)
        {
            rotateFalling();
        }
        else
        {
            if(input.left)
            {
                horizontalMove -= 1;
            }
            if(input.right)
            {
                horizontalMove += 1;
            }

            if(input.drop)
            {
                speed *= 4;
            }
        }

        for(ii in 0...mFalling.length)
        {
            var tile = mFalling[ii];
            if(tile != null)
            {
                tile.y += FlxG.elapsed * speed;
                if(collide(tile))
                {
                    mFalling[ii] = null;
                }
                else
                {
                    haveFallingBlock = true;

                    if(horizontalMove != 0 &&
                       !canMove(tile, horizontalMove, 0))
                    {
                        horizontalMove = 0;
                    }
                }
            }
        }

        if(horizontalMove != 0)
        {
            for(tile in mFalling)
            {
                if(tile != null)
                {
                    tile.x += TILE_WIDTH * horizontalMove;
                }
            }
        }

        mBadBlockTime -= FlxG.elapsed;

        if(!mDoingMatches && !haveFallingBlock)
        {
            mChainCount = 0;
            checkForMatches();
        }
    }

    private function canMove(tile : Tile, dx : Int, dy : Int) : Bool
    {
        var cell = cellPosition(tile.x, tile.y);
        cell.column += dx;
        cell.row += dy;

        if(0 > cell.column || cell.column >= COLUMNS ||
           cell.row >= ROWS)
        {
            return false;
        }

        if(cell.row >= 0 && mTiles[index(cell.row, cell.column)] != null)
        {
            return false;
        }

        // Check the bottom part of the tile too, since it might be in a
        // different row
        cell = cellPosition(tile.x, tile.y + tile.height);
        cell.column += dx;
        cell.row += dy;
        if(cell.row >= 0 && mTiles[index(cell.row, cell.column)] != null)
        {
            return false;
        }

        return true;
    }

    private function collide(tile : Tile) : Bool
    {
        var contact = false;

        // see if tile hit the bottom
        if(tile.y + tile.height > y + height)
        {
            tile.y = y + height - tile.height;
            contact = true;
        }
        else
        {
            // check if it hit another block
            var nextCell = cellPosition(tile.x, tile.y + tile.height);
            if(mTiles[index(nextCell.row, nextCell.column)] != null)
            {
                contact = true;
                tile.y = pos(nextCell.row - 1, nextCell.column).y;
            }
        }

        if(contact)
        {
            var cell = cellPosition(tile.x, tile.y);
            mTiles[index(cell.row, cell.column)] = tile;
        }

        return contact;
    }

    private function rotateFalling()
    {
        var tile1 = mFalling[0];
        var tile2 = mFalling[1];

        if(tile1 == null || tile2 == null)
        {
            return;
        }

        var cell1 = cellPosition(tile1.x, tile1.y);
        var cell2 = cellPosition(tile2.x, tile2.y);

        var dx = cell2.column - cell1.column;
        var dy = cell2.row - cell1.row;

        if(dx == 0 && dy == 0)
        {
            return;
        }

        var newDx = dy;
        var newDy = -dx;

        if(canMove(tile2, newDx, newDy))
        {
            tile2.x = tile1.x + TILE_WIDTH * newDx;
            tile2.y = tile1.y + TILE_WIDTH * newDy;
        }
    }

    private function checkForMatches()
    {
        mDoingMatches = true;

        var matchGroups = findMatchGroups();

        var tilesRemoved = false;
        for(group in matchGroups)
        {
            var length : Int = 0;
            for(tile in group)
            {
                if(!tile.isBad)
                {
                    length++;
                }
            }
            
            if(length >= 4)
            {
                mChainCount++;
                addScore(length - 3);

                for(tile in group)
                {
                    var cell = cellPosition(tile.x, tile.y);
                    mTiles[index(cell.row, cell.column)] = null;
                    tile.remove();
                    tilesRemoved = true;
                }
            }
        }

        if(tilesRemoved)
        {
            // wait for matching animations to finish
            mMatchTimer.start(Tile.DEATH_ANIMATION_DT, 1, dropTiles);
        }
        else
        {
            addPair();
        }
    }

    private function findMatchGroups() : Array<Array<Tile>>
    {
        // An IntMap to mark tiles as visited
        var visited = new IntMap<Bool>();

        var groups = new Array<Array<Tile>>();

        for(ii in 0...mTiles.length)
        {
            if(visited.get(ii))
            {
                continue;
            }

            var tile = mTiles[ii];
            visited.set(ii, true);
            if(tile == null || tile.isBad)
            {
                continue;
            }

            var group : Array<Tile> = null;
            var type : Int = tile.type;
            var neighbors = new List<Int>();
            neighbors.add(ii);
            var first = true;

            while(!neighbors.isEmpty())
            {
                var idx = neighbors.pop();

                if(!first && (visited.get(idx) ||
                              mTiles[idx] == null ||
                              (!mTiles[idx].isBad &&
                               mTiles[idx].type != type)))
                {
                    continue;
                }

                if(!first)
                {
                    if(group == null)
                    {
                        group = new Array<Tile>();
                        group.push(tile);
                    }
                    group.push(mTiles[idx]);
                    visited.set(idx, true);
                }

                first = false;

                if(!mTiles[idx].isBad)
                {
                    var cx = idx % COLUMNS;
                    var cy = Std.int(idx / COLUMNS);

                    if(cy - 1 >= 0)
                    {
                        neighbors.add(idx - COLUMNS);
                    }
                    if(cy + 1 < ROWS)
                    {
                        neighbors.add(idx + COLUMNS);
                    }
                    if(cx - 1 >= 0)
                    {
                        neighbors.add(idx - 1);
                    }
                    if(cx + 1 < COLUMNS)
                    {
                        neighbors.add(idx + 1);
                    }
                }
            }

            if(group != null)
            {
                groups.push(group);
            }
        }

        return groups;
    }

    private function dropTiles(timer)
    {
        var continuationSet = false;

        for(col in 0...COLUMNS)
        {
            var drop = 0;
            for(ii in 0...ROWS)
            {
                var row = ROWS - 1 - ii;
                var tile = mTiles[index(row, col)];
                if(tile == null)
                {
                    drop++;
                }
                else if(drop > 0)
                {
                    mTiles[index(row + drop, col)] = tile;
                    mTiles[index(row, col)] = null;

                    var clbk = continuationSet ? null : checkForMatches;
                    var dest = pos(row + drop, col);
                    var tween = new FixedLinearMotion(clbk,
                                                      FlxTween.ONESHOT);
                    tween.setMotion(tile.x, tile.y,
                                    dest.x, dest.y,
                                    DROP_DT,
                                    Ease.quadIn);
                    tween.setObject(tile);
                    addTween(tween);
                    continuationSet = true;
                }
            }
        }

        if(!continuationSet)
        {
            addPair();
        }
    }

    private function addScore(points : Int)
    {
        if(mBadBlocksWaiting > points)
        {
            mBadBlocksWaiting -= points;
        }
        else
        {
            points -= mBadBlocksWaiting;
            mBadBlocksWaiting = 0;

            if(points > 0)
            {
                cast(FlxG.state, PlayState).addBadTiles(this,
                                                        mChainCount * points);
            }
        }
    }

    inline public function addBadBlocks(count : Int)
    {
        mBadBlocksWaiting += count;
        mBadBlockTime = BAD_BLOCK_DELAY;
    }

    public function dropBadBlocks()
    {
        mDoingMatches = true;
        var continuationSet = false;

        var column : Int = 0;
        for(ii in 0...mBadBlocksWaiting)
        {
            column = (ii + mBadBlockLastColumn) % COLUMNS;
            var row = 0;

            if(mTiles[index(row, column)] != null)
            {
                lost = true;
                return;
            }

            for(jj in 0...ROWS)
            {
                row = ROWS - 1 - jj;
                if(mTiles[index(row, column)] == null)
                {
                    break;
                }
            }

            var start = pos(-1 - Std.int(ii / COLUMNS), column);
            var dest = pos(row, column);

            var tile = new Tile(Tile.BAD_BLOCK_TYPE);
            tile.x = start.x;
            tile.y = start.y;
            add(tile);
            
            var clbk = continuationSet ? null : addPair;
            var tween = new FixedLinearMotion(clbk, FlxTween.ONESHOT);
            tween.setMotion(start.x, start.y,
                            dest.x, dest.y,
                            DROP_DT, Ease.quadIn);
            tween.setObject(tile);
            addTween(tween);
            mTiles[index(row, column)] = tile;
        }

        mBadBlocksWaiting = 0;
        mBadBlockLastColumn = column + 1;
    }
}
