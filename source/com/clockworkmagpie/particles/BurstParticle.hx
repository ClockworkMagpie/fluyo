/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 * Fluyo is distributed under the terms of the GNU General Public License
 */

package com.clockworkmagpie.particles;

import org.flixel.FlxG;
import org.flixel.FlxObject;
import org.flixel.FlxParticle;

class BurstParticle extends FlxParticle
{
    // number of sprites in each row of the sheet
    static inline var SPRITE_WIDTH : Int = 7;

    // the number of sizes each color of gib has
    static inline var GIB_SIZES : Int = 4;
    
    public function new()
    {
        super();

        loadGraphic("assets/gibs.png", true, false, 10, 10);
        allowCollisions = FlxObject.NONE;
    }

    public var blockType(default, set) : Int;

    private function set_blockType(type)
    {
        frame = type + Std.random(GIB_SIZES) * SPRITE_WIDTH;
        return type;
    }
}
