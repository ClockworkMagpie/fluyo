/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 * Fluyo is distributed under the terms of the GNU General Public License
 */

package com.clockworkmagpie.particles;

import org.flixel.FlxG;
import org.flixel.FlxParticle;

class BurstEmitter extends FlxEmitterExt
{
    public var blockType : Int;

	public function new(x : Float = 0,
                        y : Float = 0,
                        size : Int = 0)
    {
        super(x, y, size);
        particleClass = BurstParticle;
    }

    /**
     * Set the particle block type before returning it.
     */
    override public function recycle(
        objectClass : Class<FlxParticle> = null,
        contructorArgs:Array<Dynamic> = null ) : FlxParticle
    {
        var particle : BurstParticle = cast super.recycle(objectClass, contructorArgs);
        particle.blockType = blockType;
        return particle;
    }
}
