/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 * Fluyo is distributed under the terms of the GNU General Public License
 */

package com.clockworkmagpie;

import org.flixel.FlxG;
import org.flixel.FlxTypedGroup;

import com.clockworkmagpie.particles.BurstEmitter;

class Gbl
{
    static private var mInitted = false;
    
    static public var burstEmitters : FlxTypedGroup<BurstEmitter>;

    static public function init()
    {
        if(!mInitted)
        {
            burstEmitters = new FlxTypedGroup<BurstEmitter>();
            burstEmitters.maxSize = 20;
        }
    }

    static public function burst(tile : Tile)
    {
        var emitter = burstEmitters.recycle(BurstEmitter);
        emitter.blockType = tile.type;
        emitter.at(tile);
        emitter.setRotation(); // no rotation
        emitter.gravity = 200;
        emitter.distance = 60;
        emitter.distanceRange = 40;
        emitter.start(true, 0.5, 0, 10);
    }
}
