package com.clockworkmagpie;

import nme.Lib;
import nme.events.Event;
import org.flixel.FlxCamera;
import org.flixel.FlxG;
import org.flixel.FlxGame;
	
class ProjectClass extends FlxGame
{	
	public function new()
	{
		var stageWidth:Int = Lib.current.stage.stageWidth;
		var stageHeight:Int = Lib.current.stage.stageHeight;

        // The stage size isn't quite right by the time this is called when it's
        // run in flashplayer
        #if flash
        stageWidth = 640;
        stageHeight = 480;
        #end
        
		var ratioX:Float = stageWidth / 320;
		var ratioY:Float = stageHeight / 240;
		var ratio:Float = Math.min(ratioX, ratioY);
		super(Math.ceil(stageWidth / ratio),
              Math.ceil(stageHeight / ratio),
              PlayState, ratio, 30, 30);
	}

    override private function create(FlashEvent:Event):Void
    {
        super.create(FlashEvent);
        stage.addEventListener(Event.RESIZE, onResize);
        onResize(null);
    }        

    private function onResize(event)
    {
        x = (stage.stageWidth - (FlxG.width * FlxCamera.defaultZoom )) / 2;
        y = (stage.stageHeight - (FlxG.height * FlxCamera.defaultZoom )) / 2;
    }
}
