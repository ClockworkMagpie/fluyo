﻿/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 * Fluyo is distributed under the terms of the GNU General Public License
 */

package com.clockworkmagpie.tweens;

import org.flixel.tweens.FlxTween;
import org.flixel.tweens.motion.LinearMotion;
import org.flixel.tweens.util.Ease;

/**
 * Determines motion along a line, from one point to another.
 */
class FixedLinearMotion extends LinearMotion
{
	/**
	 * Constructor.
	 * @param	complete	Optional completion callback.
	 * @param	type		Tween type.
	 */
	public function new(complete:CompleteCallback = null, type:Int = 0)
	{
        super(complete, type);
	}

    /**
     * Set the object to the final position when the tween is finished.
     */
    override private function finish():Void
    {
        x = _fromX + _moveX;
        y = _fromY + _moveY;
        if (_object != null)
		{
            _object.move(x, y);
		}

        super.finish();
    }
}
